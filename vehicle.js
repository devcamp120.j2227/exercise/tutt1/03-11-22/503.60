class Vehicle {
    constructor(brand,yearManufactured) {
        this.brand = brand
        this.yearManufactured = yearManufactured;
    }
    print() {
        console.log(this.brand + "-" + this.yearManufactured)
    }
}

export default Vehicle;

