import Car from "./car.js";
import Motorbike from "./motobike.js";
import Vehicle from "./vehicle.js";

let vehicle1 = new Vehicle("Vinfast", 2020);
vehicle1.print();
console.log(vehicle1 instanceof Vehicle);

let car1 = new Car("Vinfast", 2020, "A21","VF200");
car1.print();
car1.honk();
console.log(car1 instanceof Vehicle);

let motorbike1 = new Motorbike("Honda", 2021, "H21","Honda201");
motorbike1.print();
motorbike1.honk();
console.log(motorbike1 instanceof Vehicle);
