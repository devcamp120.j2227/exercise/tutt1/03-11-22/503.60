import Vehicle from "./vehicle.js";

class Motorbike extends Vehicle {
    constructor(brand,yearManufactured,vId,modelName) {
        super(brand,yearManufactured);
        this.vId = vId;
        this.modelName = modelName;
    }
    honk() {
        console.log(this.vId +"-" +this.modelName);
    }
}

export default Motorbike;